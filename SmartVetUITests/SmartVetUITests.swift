//
//  SmartVetUITests.swift
//  SmartVetUITests
//
//  Created by Mahjabin Alam on 2020/07/06.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import XCTest

class SmartVetUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSettingsAndPetsDownloadSuccess(){
        let app = XCUIApplication()
        
        let tables = app.tables
      
        let GoldfishCell = tables.staticTexts["Goldfish"]
        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: GoldfishCell, handler: nil)
        waitForExpectations(timeout: 30.0, handler: nil)
        XCTAssertTrue(GoldfishCell.exists)
    }
    
    func testPetsContentDisplaySuccess(){
        let app = XCUIApplication()
        let tables = app.tables
        let catCell = tables.staticTexts["Cat"]
        expectation(for: NSPredicate(format: "exists == 1"), evaluatedWith: catCell, handler: nil)
        waitForExpectations(timeout: 60.0, handler: nil)
        XCTAssertTrue(catCell.exists)
        catCell.tap()
        app.navigationBars["Pet Information"].buttons["Back"].tap()
    }
    
   
    func testCallAndChatButtonsActionAvailibility(){
        let userDefault = UserDefaults.standard
        let isChatEnabled = userDefault.bool(forKey: "isChatEnabled")
        
        let app = XCUIApplication()
        let chatButton = app.buttons["Chat"]
        if isChatEnabled{
            XCTAssertTrue(chatButton.exists)
        }
        else{
            XCTAssertFalse(chatButton.exists)
        }
        
        let isCallEnabled = userDefault.bool(forKey: "isCallEnabled")
        let callButton = app.buttons["Call"]
        if isCallEnabled{
            XCTAssertTrue(callButton.exists)
        }
        else{
            XCTAssertFalse(callButton.exists)
        }
    }
    
    func testIfWorkHourViewShown(){
        let userDefault = UserDefaults.standard
        let workHours = userDefault.string(forKey: "workHours")
        
        let app = XCUIApplication()
        let workingHourButton = app.buttons["Office Hours: M-F 9:00 - 18:00"]
        if workHours != nil{
            XCTAssertTrue(workingHourButton.exists)
        }
    }

    func testChatButtonDisappearsIfNotEnabled(){
        let isChatEnabled = false
        let userDefault = UserDefaults.standard
        userDefault.set(isChatEnabled, forKey: "isChatEnabled")
       
        let app = XCUIApplication()
        let chatButton = app.buttons["Chat"]
        
         XCTAssertFalse(chatButton.exists)
    }
    func testCallButtonDisappearsIfNotEnabled(){
           let isCallEnabled = false
           let userDefault = UserDefaults.standard
           userDefault.set(isCallEnabled, forKey: "isCallEnabled")
          
           let app = XCUIApplication()
           let callButton = app.buttons["Call"]
           
        XCTAssertFalse(callButton.exists)
    }
}
