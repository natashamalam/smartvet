//
//  SVSettingsTest.swift
//  SmartVetTests
//
//  Created by Mahjabin Alam on 2020/07/06.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import XCTest
@testable import SmartVet


class SVSettingsTest: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    override func tearDown() {
        super.tearDown()
    }
    
    func testSettingsInitializer(){
        let settings = Settings(isChatEnabled: true, isCallEnabled: false, workHours: "M-F 10:00-18:00")
            
        XCTAssertEqual(settings.isChatEnabled, true)
        XCTAssertEqual(settings.isCallEnabled, false)
        XCTAssertEqual(settings.workHours, "M-F 10:00-18:00")
    }
}
