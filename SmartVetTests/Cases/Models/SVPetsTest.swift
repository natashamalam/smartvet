//
//  SVPetsTest.swift
//  SmartVetTests
//
//  Created by Mahjabin Alam on 2020/07/05.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import XCTest
@testable import SmartVet

class SVPetsTest: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    override func tearDown() {
        super.tearDown()
    }
    
    func testPetsInitializer(){
        let pet = Pet(image_url: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cat_poster_1.jpg/1200px-Cat_poster_1.jpg", title: "Cat", content_url: "https://en.wikipedia.org/wiki/Cat", date_added: "2018-06-02T03:27:38.027Z")
        
        XCTAssertEqual(pet.title, "Cat")
        XCTAssertEqual(pet.image_url, "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cat_poster_1.jpg/1200px-Cat_poster_1.jpg")
        XCTAssertEqual(pet.title, "Cat")
        XCTAssertEqual(pet.date_added, "2018-06-02T03:27:38.027Z")
    }
}
