//
//  SVContactManagerTests.swift
//  SmartVetTests
//
//  Created by Mahjabin Alam on 2020/07/05.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import XCTest
@testable import SmartVet

class SVContactManagerTests: XCTestCase {
    
    let contactManager = ContactManager()
    
    func testIsOfficeHoursTest(){
        let calendar = Calendar(identifier: .gregorian)
        
        var nonOfficeHourDateComponent = DateComponents()
        nonOfficeHourDateComponent.timeZone = .current
        nonOfficeHourDateComponent.day = 6
        nonOfficeHourDateComponent.month = 7
        nonOfficeHourDateComponent.year = 2020
        nonOfficeHourDateComponent.hour = 9
        nonOfficeHourDateComponent.minute = 20
        nonOfficeHourDateComponent.second = 0
        
        if let date = calendar.date(from: nonOfficeHourDateComponent){
            XCTAssertFalse(contactManager.isOfficeHours(date))
        }
        
        var officeHourDateComponent = DateComponents()
        officeHourDateComponent.timeZone = .current
        officeHourDateComponent.day = 6
        officeHourDateComponent.month = 7
        officeHourDateComponent.year = 2020
        officeHourDateComponent.hour = 13
        officeHourDateComponent.minute = 20
        officeHourDateComponent.second = 0
        
        if let date = calendar.date(from: officeHourDateComponent){
            XCTAssertTrue(contactManager.isOfficeHours(date))
        }
    }
    
    
}
