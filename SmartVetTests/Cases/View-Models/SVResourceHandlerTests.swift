//
//  SVResourceHandlerTests.swift
//  SmartVetTests
//
//  Created by Mahjabin Alam on 2020/07/06.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import XCTest
@testable import SmartVet

class SVResourceHandlerTests: XCTestCase {

    let resourceHandler = ResourceHandler()
    
    func testDownloadSettingsConfig(){
        let url: URL? = nil // default nil
        resourceHandler.downloadSettingsConfig(url)
        
        let isChatEnabled = UserDefaults.standard.bool(forKey: "isChatEnabled")
        XCTAssertNotNil(isChatEnabled)
        
        let isCallEnabled = UserDefaults.standard.bool(forKey: "isCallEnabled")
        XCTAssertNotNil(isCallEnabled)
        
        let workHours = UserDefaults.standard.string(forKey: "workHours")
        XCTAssertNotNil(workHours)
    }
    
    func testDownloadPetsData(){
        let url: URL? = nil // default url
        let pets = resourceHandler.downloadPetsData(url)
        XCTAssertGreaterThan(pets.count, 0)
    }

}
