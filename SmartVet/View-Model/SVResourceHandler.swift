//
//  ResourceHandler.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 3/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import Foundation


class ResourceHandler {
    
    let decoder = JSONDecoder()
    
    var error :((_ error : Error)->())?
    var warningMessage: ((_ msg : String)->())?
    
    public func downloadSettingsConfig(_ configURL: URL?){
        let contentString: String
        var settings: Settings
        var url: URL
        
        if configURL != nil {
            url = configURL!
        }
        else if settingsURL != nil{
            url = settingsURL!
        }
        else{
            warningMessage?("Config URL can not be Found!")
            return
        }
        do{
            contentString = try String(contentsOf: url)
            if let contentStringData = contentString.data(using: .utf8){
                do{
                    settings = try decoder.decode(Settings.self, from: contentStringData)
                    loadSettingsIntoUserDefault(settings)
                }
                catch let decoderError{
                    error?(decoderError)
                }
            }
            else{
                warningMessage?("URL Data Generation Failed!")
            }
        }catch let urlError{
            error?(urlError)
        }
    }
    
    public func downloadPetsData(_ remotePetURL:URL?)->[Pet]{
        let contentString: String
        var pets = [Pet]()
        
        var url: URL
               
        if remotePetURL != nil {
            url = remotePetURL!
        }
        else if petURL != nil{
            url = petURL!
        }
        else{
            warningMessage?("Pets URL can not be Found!")
            return []
        }
        do{
            contentString = try String(contentsOf: url)
            if let contentStringData = contentString.data(using: .utf8){
                do{
                    pets = try decoder.decode([Pet].self, from: contentStringData)
                }
                catch let decoderError{
                    error?(decoderError)
                }
            }
            else{
                warningMessage?("URL Data Generation Failed!")
            }
        }catch let urlError{
            error?(urlError)
        }
        
        return pets
    }
}

    // MARK: Save into User defaults
extension ResourceHandler{
    
    public func isSavedIntoUserDefault()->Bool{
        if workHourFromUserDefaults() != nil && chatAbilityFromUserDefaults() != nil && callAbilityFromUserDefaults() != nil{
            return true
        }
        return false
    }
    public func loadSettingsIntoUserDefault(_ settings:Settings){
        let userDefault = UserDefaults.standard
        userDefault.set(settings.isChatEnabled, forKey: "isChatEnabled")
        userDefault.set(settings.isCallEnabled, forKey: "isCallEnabled")
        userDefault.set(settings.workHours, forKey: "workHours")
    }
    
    public func workHourFromUserDefaults()->String?{
        let userDefault = UserDefaults.standard
        let workHours = userDefault.string(forKey: "workHours")
        return workHours ?? nil
    }
    public func chatAbilityFromUserDefaults()->Bool?{
        let userDefault = UserDefaults.standard
        let chatEnabled = userDefault.bool(forKey: "isChatEnabled")
        return chatEnabled
    }
    public func callAbilityFromUserDefaults()->Bool?{
        let userDefault = UserDefaults.standard
        let callEnabled = userDefault.bool(forKey: "isCallEnabled")
        return callEnabled
    }

}
extension ResourceHandler{
    
    private struct RemoteLinks{
        static let remotePetsURLString = "http://dev-realm.com/pets.json"
        static let remoteConfigURLString = "http://dev-realm.com/config.json"
    }
    
    private var petURL: URL?{
        guard let url = URL(string: RemoteLinks.remotePetsURLString)
            else{
                return nil
            }
        return url
    }
        
    private var settingsURL: URL?{
        guard let url = URL(string: RemoteLinks.remoteConfigURLString)
            else{
                return nil
            }
        return url
    }
}
