//
//  ViewHelper.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 5/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import Foundation
import UIKit

class ViewHelper{
    
    let handler = ResourceHandler()
    
    struct Constants {
        static let petTableViewCellHeight: CGFloat = 110.0
        static let buttonCornerRadiusMultiplier: CGFloat = 0.25
        static let officeHoursBorderWidth: CGFloat = 2.0
        static let colorGray: UIColor = UIColor.lightGray
    }
    
    func alertController(withTitle title:String, message:String?, andActionStyle: UIAlertAction.Style)->UIAlertController{
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(alertAction)
        return alert
    }
    
    func workingHourAlert()->UIAlertController{
         let msgAlert = alertController(withTitle: "Welcome to SmartVet", message: "Thank you for getting in touch with us. We’ll get back to you as soon as possible", andActionStyle: .cancel)
        return msgAlert
    }
    
    func afterHourAlert()->UIAlertController{
        let msgAlert = alertController(withTitle: "Sorry", message: "Work hours has ended. Please contact us again on the next work day", andActionStyle: .cancel)
        return msgAlert
    }
    func shouldRemoveBothButtons()->Bool{
        if let chatEnabled = handler.chatAbilityFromUserDefaults(), chatEnabled == false{
            if let callEnabled = handler.callAbilityFromUserDefaults(), callEnabled == false{
                    return true
            }
        }
        return false
    }
    func shouldRemoveChatButton()->Bool{
        if let chatEnabled = handler.chatAbilityFromUserDefaults(), chatEnabled == false{
           return true
        }
        return false
    }
    
    func shouldRemoveCallButton()->Bool{
        if let callEnabled = handler.callAbilityFromUserDefaults(), callEnabled == false{
            return true
        }
        return false
    }

    func officeHourString()->String{
        var hours = "Office Hours: "
        if let officeHours = handler.workHourFromUserDefaults(){
            hours = hours + officeHours
        }
        return hours
    }
}
