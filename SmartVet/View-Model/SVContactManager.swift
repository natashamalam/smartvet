//
//  SVContactLogicHandler.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 5/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import Foundation

class ContactManager{
    
    private func getCalender()->Calendar{
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = NSLocale(localeIdentifier: "UTC") as Locale
        calendar.timeZone = .current
        return calendar
    }
    
    private func withinWorkingHours(_ currentDayAndTime: Date)->Bool{
        let calendar = getCalender()
        let date = currentDayAndTime
        let hour = calendar.component(.hour, from: date)
        if hour >= 10 && hour <= 18{
            return true
        }
        return false
    }
    
    private func isWorkingDay(_ currentDayAndTime: Date)->Bool{
        let calendar = getCalender()
        let date = currentDayAndTime
        let weekDay = calendar.component(.weekday, from: date)
       
        if weekDay > 1 && weekDay < 7{
            return true
        }
        return false
    }
    
    func isOfficeHours(_ currentDayAndTime: Date)->Bool{
        if isWorkingDay(currentDayAndTime) && withinWorkingHours(currentDayAndTime){
            return true
        }
        return false
    }
    
}
