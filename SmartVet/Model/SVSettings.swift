//
//  SVSettings.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 3/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import Foundation

struct Settings:Codable, CustomStringConvertible {
   
    var isChatEnabled: Bool
    var isCallEnabled: Bool
    var workHours: String
    
    var description: String {
        get{
            return "chat enabled: \(String(describing: isChatEnabled)),\n call enabled: \(String(describing: isCallEnabled)),\n work hours: \(String(describing: workHours))"
        }
    }
    
}
