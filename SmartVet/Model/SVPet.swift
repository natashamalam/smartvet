//
//  SVPet.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 3/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import Foundation

struct Pet: Codable, CustomStringConvertible{
    
    var description: String{
        return "image_url: \(String(describing:image_url))\n title: \(String(describing:title)) \n content_url: \(String(describing:content_url))\n date_added: \(String(describing:date_added))\n"
    }
    
    let image_url: String
    let title: String
    let content_url: String
    let date_added: String
    
}
