//
//  PetDetailsViewController.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 4/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import UIKit
import WebKit

class SVPetDetailsViewController: UIViewController, WKNavigationDelegate {

    var urlString:String?
    var contentFetchingErrorMessage : ((_ errorMessage:String)->())?
    
    
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    @IBOutlet weak var petDetailsWebKitView: WKWebView!{
        didSet{
            petDetailsWebKitView.navigationDelegate = self
            if urlString != nil{
                if let url = URL(string: urlString!){
                    loadWebViewWith(url)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadingWheel.startAnimating()
    }
    
    func loadWebViewWith(_ url:URL){
        let request = URLRequest(url: url)
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request){ (data, response, error) in
            if let dataTaskError = error{
                self.contentFetchingErrorMessage?(dataTaskError.localizedDescription)
            }
            else{
                var statusCode: Int
                if let urlResponse = response as? HTTPURLResponse{
                    statusCode = urlResponse.statusCode
                    if statusCode == 200 {
                        if let contentData = data{
                                DispatchQueue.main.async { [weak self] in
                                    if let contentString = String(data: contentData, encoding: .utf8){
                                        self?.petDetailsWebKitView.loadHTMLString(contentString, baseURL: urlResponse.url)
                                    }
                                    else{
                                         self?.contentFetchingErrorMessage?("Content Could Not be Generated.")
                                    }
                            }
                        }
                        else{
                            self.contentFetchingErrorMessage?("Content Could Not be Generated.")
                        }
                    }
                    else{
                        self.contentFetchingErrorMessage?("Bad Access!")
                    }
                }
            }
        }
        dataTask.resume()
        
        self.petDetailsWebKitView.load(request)
    }
    
}
extension SVPetDetailsViewController{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        self.loadingWheel.stopAnimating()
    }
}
