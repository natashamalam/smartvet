//
//  SVContactViewController.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 1/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import UIKit

class SVContactViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISplitViewControllerDelegate {
    
    let handler = ResourceHandler()
    let viewHelper = ViewHelper()
    let contactManger = ContactManager()
    
    var pets = [Pet]()
    
    @IBOutlet weak var chatButton: SVRoundedCornerButton!
    @IBOutlet weak var callButton: SVRoundedCornerButton!
    @IBOutlet weak var buttonHolderView: UIView!
    
    @IBOutlet weak var tableViewAppearingActivityIndicator:UIActivityIndicatorView!
    
    @IBOutlet weak var petTableView: UITableView!
    @IBOutlet weak var officeHoursButton: UIButton!{
        didSet{
            configureOfficeHourButtonView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        splitViewController?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handler.error = resourceFetchingError
        handler.warningMessage = resourceFetchingWarningMessage
        
        if !handler.isSavedIntoUserDefault(){
            handler.downloadSettingsConfig(nil)
        }
        officeHoursButton.setTitle(viewHelper.officeHourString(), for: .normal)
        configureButtons()
        
        DispatchQueue.global(qos: .background).async {
            self.pets = self.handler.downloadPetsData(nil)
            DispatchQueue.main.async {
                self.petTableView.reloadData()
                self.tableViewAppearingActivityIndicator.stopAnimating()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableViewAppearingActivityIndicator.startAnimating()
    }
     // MARK: Button action methods
    @IBAction func clickedChatButton(_ sender: SVRoundedCornerButton) {
        var alert : UIAlertController
        if contactManger.isOfficeHours(Date()){
            alert = viewHelper.workingHourAlert()
        }
        else{
            alert = viewHelper.afterHourAlert()
        }
        self.present(alert, animated: true)
    }
    @IBAction func clickedCallButton(_ sender: SVRoundedCornerButton) {
        var alert : UIAlertController
        if contactManger.isOfficeHours(Date()){
            alert = viewHelper.workingHourAlert()
        }
        else{
            alert = viewHelper.afterHourAlert()
        }
        self.present(alert, animated: true)
    }
    
    // MARK: Utility methods
    
    private func configureButtons(){
        if viewHelper.shouldRemoveBothButtons(){
            buttonHolderView.removeFromSuperview()
        }
        if viewHelper.shouldRemoveChatButton(){
            chatButton.removeFromSuperview()
        }
        if viewHelper.shouldRemoveCallButton(){
            callButton.removeFromSuperview()
        }
    }
    private func configureOfficeHourButtonView(){
        officeHoursButton.layer.borderWidth = ViewHelper.Constants.officeHoursBorderWidth
        officeHoursButton.layer.borderColor = ViewHelper.Constants.colorGray.cgColor
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segueIdentifier = segue.identifier, segueIdentifier == "petDetailsSegue"{
            if let cell = sender as? SVPetTableViewCell{
                if let navVC = segue.destination as? UINavigationController{
                    if let vc = navVC.visibleViewController as? SVPetDetailsViewController{
                        if let indexPath = petTableView.indexPath(for: cell){
                            let pet = pets[indexPath.row]
                            vc.urlString = pet.content_url
                            vc.contentFetchingErrorMessage = contentFetchingErrorMessage
                        }
                    }
                    else{
                        let warning = viewHelper.alertController(withTitle: "Sorry", message: "Destination Controller Can not be Reached", andActionStyle: .cancel)
                        self.present(warning, animated: true)
                    }
                }
            }
        }
    }
}

// MARK: Call Back methods
extension SVContactViewController{
    
    // MARK: from resource handler
    
    func resourceFetchingError(_ error:Error){
        DispatchQueue.main.async { [weak self] in
            if let alert = self?.viewHelper.alertController(withTitle: "Failed Resource Fetching!", message: error.localizedDescription, andActionStyle: .cancel){
                self?.present(alert, animated: true)
            }
        }
    }
    
    func resourceFetchingWarningMessage(_ msg : String){
         DispatchQueue.main.async { [weak self] in
            if let alert = self?.viewHelper.alertController(withTitle: "Sorry", message: msg, andActionStyle: .cancel){
                 self?.present(alert, animated: true)
            }
        }
    }
    
    // MARK: for image_url load
      
    func imageFetchingFailed(_ error: Error){
        DispatchQueue.main.async { [weak self] in
            if let alert = self?.viewHelper.alertController(withTitle: "Sorry", message: error.localizedDescription, andActionStyle: .cancel){
                self?.present(alert, animated: true)
            }
        }
    }
    // MARK: content_url load
    
    func contentFetchingErrorMessage(_ errorString: String){
        DispatchQueue.main.async { [weak self] in
            if let alert = self?.viewHelper.alertController(withTitle: "Failure in Loading", message: errorString, andActionStyle: .cancel){
                self?.present(alert, animated: true)
            }
        }
    }
}
// MARK: TableView Delegate and Datasource

extension SVContactViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return pets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "petInfoCell") as! SVPetTableViewCell
        let pet = pets[indexPath.row]
        cell.petTitleLabel.text = pet.title
        cell.petImageURLString = pet.image_url
        cell.imageFetchingError = imageFetchingFailed
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ViewHelper.Constants.petTableViewCellHeight
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let petCell = cell as? SVPetTableViewCell{
            petCell.petImageURLString = nil
        }
    }
}

extension SVContactViewController{
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool{
        return true
    }

}
