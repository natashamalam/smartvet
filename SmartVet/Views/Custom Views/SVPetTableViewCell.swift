//
//  SVPetTableViewCell.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 4/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import UIKit


class SVPetTableViewCell: UITableViewCell {
    
    let viewHelper = ViewHelper()

    var petImageURLString:String? = nil{
        didSet{
            setNeedsLayout()
        }
    }
    var imageFetchingError : ((_ error:Error)->())?
    
    @IBOutlet weak var petImageLoaderIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var petTitleLabel: UILabel!
    @IBOutlet weak var petImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.petImageView.image = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let urlString = petImageURLString{
            loadImage(from: urlString)
        }
        else{
            petImageView.image = UIImage(named: "no_image")
        }
    }
    
    private func loadImage(from imageURLString: String){
           if let url = URL(string: imageURLString){
                petImageLoaderIndicator.startAnimating()
            
            let session = URLSession.shared
            session.configuration.timeoutIntervalForResource = 30.0
            let task = session.dataTask(with: url) { (data, response, error) in
                if let dataTaskError = error{
                    self.imageFetchingError?(dataTaskError)
                }
                else{
                    if let downloadResponse = response as? HTTPURLResponse{
                       if downloadResponse.statusCode == 200{
                           if let imageData = data{
                            if let image = UIImage(data: imageData){
                                DispatchQueue.main.async{ [weak self] in
                                    self?.petImageView.image = image
                                    self?.petImageLoaderIndicator.stopAnimating()
                                }
                            }
                           }
                       }
                       else if downloadResponse.statusCode == 404{
                            print("Bad request. Image Cannot be fetched.")
                        }
                       else{
                            print(HTTPURLResponse.localizedString(forStatusCode: downloadResponse.statusCode))
                        }
                    }
                }
           }
            task.resume()
           }
    }
}
