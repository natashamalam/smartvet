//
//  SVRoundedCornerButton.swift
//  SmartVet
//
//  Created by Mahjabin Alam on 4/7/20.
//  Copyright © 2020 Mahjabin Alam. All rights reserved.
//

import UIKit

extension SVRoundedCornerButton{
    private static let cornerRadiusMultiplier: CGFloat = 0.25
}

class SVRoundedCornerButton: UIButton {

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.masksToBounds = true
        layer.cornerRadius = bounds.height * ViewHelper.Constants.buttonCornerRadiusMultiplier
    }

}
